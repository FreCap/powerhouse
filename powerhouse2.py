from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/info/<int:level_battery>/<int:curr_consumption>/<int:time>', methods=['GET'])
def info(level_battery, curr_consumption, time):
    return jsonify({'charge':True})


if __name__ == '__main__':
    app.run()
